File.delete("debian/Gemfile.lock") if File.exist?("debian/Gemfile.lock")
require 'gem2deb/rake/testtask'

ENV["BUNDLE_GEMFILE"] = File.expand_path("Gemfile", __dir__)

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.ruby_opts << "-rbundler/setup"
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb']
end
